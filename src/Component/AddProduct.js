import react, { useState } from 'react';
import { FormGroup, makeStyles } from '@material-ui/core';
import { addProduct} from '../Service/api';
import { useHistory } from 'react-router-dom';

const initialValue = {
    name: '',
    category: '',
    price: '',
    description: ''
}

const useStyles = makeStyles({
    container: {
        width: '50%',
        margin: '5% 0 0 25%',
        '& > *': {
            marginTop: 20
        }
    }
})

const AddProduct = () => {
    const [product, setProduct] = useState(initialValue);
    const { name, category, price, description } = product;
    const classes = useStyles();
    let history = useHistory();

    const onValueChange = (e) => {
        console.log(e.target.value);
        setProduct({...product, [e.target.name]: e.target.value})
    }

    const addProductDetails = async() => {
        await addProduct(product);
        history.push('/all');
        console.log(product);
    }

    return (
        <FormGroup className={classes.container}>
            <h2 style={{
                textAlign:"center"
            }}>Nouveau Produit</h2>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Nom</span>
                <input onChange={(e) => onValueChange(e)} name='name' value={name} type="text" class="form-control" aria-describedby="basic-addon1" />
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Prix</span>
                <input onChange={(e) => onValueChange(e)} type='number' name='price' value={price} class="form-control" aria-describedby="basic-addon1"/>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Categorie</span>
                <input onChange={(e) => onValueChange(e)} name='category' value={category} type="text" class="form-control" aria-describedby="basic-addon1"/>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Description </span>
                <input onChange={(e) => onValueChange(e)} name='description' value={description} type="text" class="form-control" aria-describedby="basic-addon1" />
            </div>
            <div class="input-group mb-3">
                <button type="button" class="btn btn-secondary form-control" onClick={() => addProductDetails()}>Ajouter Produit</button>
            </div>
        </FormGroup>
    )
}

export default AddProduct;