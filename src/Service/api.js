import axios from 'axios';

const productsUrl = 'http://localhost:8000/products';


export const getProducts = async (id) => {
    id = id || '';
    return await axios.get(`${productsUrl}/${id}`);
}

export const addProduct = async (product) => {
    return await axios.post(productsUrl, product);
}

export const deleteProduct = async (id) => {
    return await axios.delete(`${productsUrl}/${id}`);
}

export const editProduct = async (id, product) => {
    return await axios.put(`${productsUrl}/${id}`, product)
}